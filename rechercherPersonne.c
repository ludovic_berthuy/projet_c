#include<stdio.h>
#include<stdlib.h>
#include"partie1.h"

 /*
        Chercher une personne dans l'annuaire
 */
void rechercherPersonne(ANNUAIRE a, int nombre_personnes){

        // nom à chercher
        char nom[30];
        // si le mot est trouvé
        int trouve = 0;

        printf("\n ________________________________");
        printf("\n __   RECHERCHER UNE PERSONNE  __");
        printf("\n ________________________________");

        // verifier si l'annuaire est vide !
        if(nombre_personnes == 0 ){
                printf("\n\n L'annuaire est vide! (%d) \n", TAILLE_ANNUAIRE);
                return;
        }

        // lecture nom à chercher
        printf("\n N.B : les noms, prenoms doubles ne doivent pas contenir des espaces!");
        printf("\n       merci de les relier avec un - !");
        printf("\n");
        printf("\n . Nom de la personne à chercher : ");
        scanf("%s", nom);

        // chercher dans l'annuaire :
        for (int i = 0; i < nombre_personnes; ++i){
                // verifier le nom de chaque personne
                if(strcmp(nom, a[i].nom) == 0){
                        // personne trouvée :
			printf("\n\tnom : %s  \tprenom : %s  \ttel : %s \n",a[i].nom, a[i].prenom, a[i].tel);
                        trouve = 1;
                }
        }
        if(trouve == 0) {
                // ici la personne n'est pas trouvée dans l'annuaire
		printf("\n Aucune personne avec le nom '%s' n'est trouvée dans l'annuaire !\n\n",nom );
        }
}

