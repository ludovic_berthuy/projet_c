#include<stdio.h>
#include<stdlib.h>
#include"partie1.h"

/*
        Ajouter une personne à l'annuaire, vers la fin
        la fonction augmente le nombre de personne si la personne est bien ajoutée
 */
void ajouterPersonne(ANNUAIRE a, int *nombre_personnes){

        // lecture nom , prenom et tel d'une personne
        printf("\n _____________________________");
        printf("\n __   AJOUTER UNE PERSONNE  __");
        printf("\n _____________________________");

        // verifier si on a atteint la limite de l'annuaire (100)
        if(*nombre_personnes == TAILLE_ANNUAIRE ){
                printf("\n\n L'annuaire est plein! (%d) \n", TAILLE_ANNUAIRE);
                return;
        }

        printf("\n N.B : les noms, prenoms doubles ne doivent pas contenir des espaces!");
        printf("\n       merci de les relier avec un - !");
        printf("\n");
        printf("\n . Nom de la personne : ");
        scanf("%s", a[*nombre_personnes].nom);
        printf("\n . Prénom de la personne : ");
        scanf("%s", a[*nombre_personnes].prenom);
        printf("\n . Telephone de la personne : ");
        scanf("%s", a[*nombre_personnes].tel);

        // augmenter le nombre de personnes
        (*nombre_personnes)++;
}


