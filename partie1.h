#ifndef partie1_H
#define partie1_H

// definir le chemin de l'annuaire
#define CHEMIN_ANNUAIRE "annuaire.txt"
#define TAILLE_ANNUAIRE 100

//  la structure suivante pour stocker les données d’une personne :
typedef struct {
        char nom[30]; // Nom de la personne
        char prenom[30]; // Prenom de la personne
        char tel[13]; // Tel de la personne
        } PERSONNE;

// la structure annuaire qui contient au plus 100 personnes
typedef PERSONNE ANNUAIRE[TAILLE_ANNUAIRE];


void ecriture(char nom[256]);

void write_diff(char nom[256], int var[256]);

void lecture(char nom[256], char* chaine);

void cryptage(char* source, char* perroquet);

void decryptage(char* source, char* perroquet);

void supp_etoile(char chaine[256], int* tab);

void chargerAnnuaire(ANNUAIRE a, int* nombre_personnes);
void afficherAnnuaire(ANNUAIRE a, int nombre_personnes);
void sauvegarderAnnuaire(ANNUAIRE a, int nombre_personnes);
void ajouterPersonne(ANNUAIRE a, int *nombre_personnes);
void rechercherPersonne(ANNUAIRE a, int nombre_personnes);
void gererAnnuaire(ANNUAIRE a, int *nombre_personnes);


#endif /* partie1_H */


