#include<stdio.h>
#include<stdlib.h>
#include"partie1.h"

/*
        Sauvegarder la liste des personne dans l'annuaire dans le fichier
        annuaire
*/
void afficherAnnuaire(ANNUAIRE a, int nombre_personnes){

        printf("\n _____________________________\n");
        printf(" ___        ANNUAIRE        __\n");
        printf(" _____________________________\n");
        // ecreiture des personne une par une
        for (int i = 0; i < nombre_personnes; ++i){
                // afficher une parsonne par ligne
                printf("%s %s %s\n", a[i].nom, a[i].prenom, a[i].tel);
        }
        // si annuaire vide
        if(nombre_personnes == 0){
                printf("\n Vide !");
        }
        printf("\n\n");
}

