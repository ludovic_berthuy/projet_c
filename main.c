#include<stdio.h>
#include<stdlib.h>
#include"partie1.h"
#include<string.h>
#include"ecriture.c"
#include"lecture.c"
#include"cryptage.c"
#include"decryptage.c"

int main (void)
{
char source[256]="source.txt";
char perroquet[256]="perroq.txt";
char dest[256]="dest.crt";

char* perroquet_chaine;
char* source_chaine;
char* dest_chaine;

perroquet_chaine = ( char * ) malloc( 256*sizeof( char ) );
source_chaine = ( char * ) malloc( 256*sizeof( char ) );
dest_chaine = ( char * ) malloc( 256*sizeof( char ) );

ecriture(source);
ecriture(perroquet);

lecture(source, source_chaine);
lecture(perroquet, perroquet_chaine);

cryptage(source_chaine, perroquet_chaine);
lecture(dest, dest_chaine);

//decryptage(dest_chaine, perroquet_chaine);

//lecture(dest, dest_chaine);


free(perroquet_chaine);
free(source_chaine);
free(dest_chaine);
return 0;
}


