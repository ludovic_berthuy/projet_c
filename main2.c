
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
// definir le chemin de l'annuaire
#define CHEMIN_ANNUAIRE "annuaire.txt"
#define TAILLE_ANNUAIRE 100

//  la structure suivante pour stocker les données d’une personne :
typedef struct { 
	char nom[30]; // Nom de la personne
	char prenom[30]; // Prenom de la personne
	char tel[13]; // Tel de la personne
	} PERSONNE;

// la structure annuaire qui contient au plus 100 personnes 
typedef PERSONNE ANNUAIRE[TAILLE_ANNUAIRE];


/*
	Charger la liste des personne dans l'annuaire depuis le fichier 
	annuaire 
	Les données sont sauvegardé dans a et nombre_personnes
*/
void chargerAnnuaire(ANNUAIRE a, int* nombre_personnes){

	// lecture du ficheir annuaire en mode "r"
	FILE * fichier = fopen(CHEMIN_ANNUAIRE, "r");
	// compteur des personnes 
	int i = 0;
	// recuperer des personne ligne par ligne
	while(feof(fichier) == 0){
		// récupétet une personne par ligne
		fscanf(fichier,"%s %s %s\n", a[i].nom, a[i].prenom, a[i].tel);
		i++;
		// mettre à jour le nombre de personnes 
		(*nombre_personnes)++;
	}

	// fermier le fichier
	fclose(fichier);
}

/*
	Sauvegarder la liste des personne dans l'annuaire dans le fichier 
	annuaire 
*/
void afficherAnnuaire(ANNUAIRE a, int nombre_personnes){

	printf("\n _____________________________\n");
	printf(" ___        ANNUAIRE        __\n");
	printf(" _____________________________\n");
	// ecreiture des personne une par une 
	for (int i = 0; i < nombre_personnes; ++i){
		// afficher une parsonne par ligne
		printf("%s %s %s\n", a[i].nom, a[i].prenom, a[i].tel);
	}
	// si annuaire vide 
	if(nombre_personnes == 0){
		printf("\n Vide !");
	}
	printf("\n\n");
}

/*
	Sauvegarder la liste des personne dans l'annuaire dans le fichier 
	annuaire 
*/
void sauvegarderAnnuaire(ANNUAIRE a, int nombre_personnes){
	// mettre à jour du ficheir annuaire
	FILE * fichier = fopen(CHEMIN_ANNUAIRE, "w");

	// ecreiture des personne une par une 
	for (int i = 0; i < nombre_personnes; ++i){
		// ajouter une personne au fichier par ligne
		fprintf(fichier,"%s %s %s\n", a[i].nom, a[i].prenom, a[i].tel);
	}
	// fermier le fichier
	fclose(fichier);
}


/* 
	Ajouter une personne à l'annuaire, vers la fin
	la fonction augmente le nombre de personne si la personne est bien ajoutée
 */
void ajouterPersonne(ANNUAIRE a, int *nombre_personnes){

	// lecture nom , prenom et tel d'une personne
	printf("\n _____________________________");
	printf("\n __   AJOUTER UNE PERSONNE  __");
	printf("\n _____________________________");

	// verifier si on a atteint la limite de l'annuaire (100)
	if(*nombre_personnes == TAILLE_ANNUAIRE ){
		printf("\n\n L'annuaire est plein! (%d) \n", TAILLE_ANNUAIRE);
		return;
	}

	printf("\n N.B : les noms, prenoms doubles ne doivent pas contenir des espaces!");
	printf("\n       merci de les relier avec un - !");
	printf("\n");
	printf("\n . Nom de la personne : ");
	scanf("%s", a[*nombre_personnes].nom);
	printf("\n . Prénom de la personne : ");
	scanf("%s", a[*nombre_personnes].prenom);
	printf("\n . Telephone de la personne : ");
	scanf("%s", a[*nombre_personnes].tel);

	// augmenter le nombre de personnes
	(*nombre_personnes)++;
}


/* 
	Chercher une personne dans l'annuaire
 */
void rechercherPersonne(ANNUAIRE a, int nombre_personnes){

	// nom à chercher
	char nom[30];
	// si le mot est trouvé
	int trouve = 0;

	printf("\n ________________________________");
	printf("\n __   RECHERCHER UNE PERSONNE  __");
	printf("\n ________________________________");

	// verifier si l'annuaire est vide !
	if(nombre_personnes == 0 ){
		printf("\n\n L'annuaire est vide! (%d) \n", TAILLE_ANNUAIRE);
		return;
	}

	// lecture nom à chercher
	printf("\n N.B : les noms, prenoms doubles ne doivent pas contenir des espaces!");
	printf("\n       merci de les relier avec un - !");
	printf("\n");
	printf("\n . Nom de la personne à chercher : ");
	scanf("%s", nom);

	// chercher dans l'annuaire :
	for (int i = 0; i < nombre_personnes; ++i){
		// verifier le nom de chaque personne
		if(strcmp(nom, a[i].nom) == 0){
			// personne trouvée :
			printf("\n\tnom : %s  \tprenom : %s  \ttel : %s \n",a[i].nom, a[i].prenom, a[i].tel);
			trouve = 1;
		}
	}
	if(trouve == 0) {
		// ici la personne n'est pas trouvée dans l'annuaire 
		printf("\n Aucune personne avec le nom '%s' n'est trouvée dans l'annuaire !\n\n",nom );
	}
}

/*
	Charger l'annuaire, 
	Afficher un menu et choisir options pour gérer l'annuaire 
	L'annuaire est bien stocké à a, et le nombre à nombre_personnes
*/
void gererAnnuaire(ANNUAIRE a, int *nombre_personnes) 
{

	int choix = -1;

	// chargement de l'annuaire 
	chargerAnnuaire(a, nombre_personnes);

	do{
		// afficher menu 
		printf("\n____________ Bienvenu _________");
		printf("\n\t1. Afficher Annuaire ");
		printf("\n\t2. Ajouter une personne ");
		printf("\n\t3. Rechercher");
		// printf("\n\t4. Crypter l'annuaire");
		printf("\n\t4. Quitter ");
		printf("\n Votre choix est bien : ");
		scanf("%d", & choix);
		switch(choix){
			case 1 :
				afficherAnnuaire(a, *nombre_personnes);
				break;
			case 2 :
				// la fonction modifier nombre personne et annuaire
				ajouterPersonne(a, nombre_personnes);
				break;
			case 3 : 
				rechercherPersonne(a, *nombre_personnes);
				break;
			case 4 : 
				// sortie 
				// sauvegarder le fichier 
				sauvegarderAnnuaire(a, *nombre_personnes);
				return ;
			default :
				// un choix non valide
				printf("\n Le choix introduit n'est pas valide ! \n");
				break;
		}
	}while(choix != 4); // boucler jusqu'à sortir
}

/*
	Afficher menu et choisir options pour gérer l'annuaire 
*/
int main(int argc, char* argv[]) 
{
	ANNUAIRE annuaire;
	int nombre_personnes = 0;

	// gestion de l'annuaire :
	gererAnnuaire(annuaire, &nombre_personnes);

}
