#include<stdio.h>
#include<stdlib.h>
#include"partie1.h"

/*
        Sauvegarder la liste des personne dans l'annuaire dans le fichier
        annuaire
*/


void sauvegarderAnnuaire(ANNUAIRE a, int nombre_personnes){
        // mettre à jour du ficheir annuaire
        FILE * fichier = fopen(CHEMIN_ANNUAIRE, "w");

        // ecreiture des personne une par une
        for (int i = 0; i < nombre_personnes; ++i){
                // ajouter une personne au fichier par ligne
                fprintf(fichier,"%s %s %s\n", a[i].nom, a[i].prenom, a[i].tel);
        }
        // fermier le fichier
        fclose(fichier);
}

