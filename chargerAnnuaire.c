#include<stdio.h>
#include<stdlib.h>
#include"partie1.h"

/*
        Charger la liste des personne dans l'annuaire depuis le fichier
        annuaire
        Les données sont sauvegardé dans a et nombre_personnes
*/
void chargerAnnuaire(ANNUAIRE a, int* nombre_personnes){

        // lecture du ficheir annuaire en mode "r"
        FILE * fichier = fopen(CHEMIN_ANNUAIRE, "r");
        // compteur des personnes
        int i = 0;
        // recuperer des personne ligne par ligne
        while(feof(fichier) == 0){
                // récupétet une personne par ligne
                fscanf(fichier,"%s %s %s\n", a[i].nom, a[i].prenom, a[i].tel);
                i++;
                // mettre à jour le nombre de personnes
                (*nombre_personnes)++;
        }

        // fermier le fichier
        fclose(fichier);
}

