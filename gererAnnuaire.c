#include<stdio.h>
#include<stdlib.h>
#include"partie1.h"
#include"chargerAnnuaire.c"
#include"afficherAnnuaire.c"
#include"ajouterPersonne.c"
#include"rechercherPersonne.c"
#include"sauvegarderAnnuaire.c"



/*
        Charger l'annuaire,
        Afficher un menu et choisir options pour gérer l'annuaire
        L'annuaire est bien stocké à a, et le nombre à nombre_personnes
*/
void gererAnnuaire(ANNUAIRE a, int *nombre_personnes)
{

        int choix = -1;

        // chargement de l'annuaire
        chargerAnnuaire(a, nombre_personnes);

        do{
                // afficher menu
                printf("\n____________ Bienvenu _________");
                printf("\n\t1. Afficher Annuaire ");
                printf("\n\t2. Ajouter une personne ");
                printf("\n\t3. Rechercher");
                // printf("\n\t4. Crypter l'annuaire");
                printf("\n\t4. Quitter ");
                printf("\n Votre choix est bien : ");
                scanf("%d", & choix);
                switch(choix){
                        case 1 :
                                afficherAnnuaire(a, *nombre_personnes);
                                break;
                        case 2 :
                                // la fonction modifier nombre personne et annuaire
                                ajouterPersonne(a, nombre_personnes);
                                break;
                        case 3 :
                                rechercherPersonne(a, *nombre_personnes);
                                break;
                        case 4 :
                                // sortie
                                // sauvegarder le fichier
                                sauvegarderAnnuaire(a, *nombre_personnes);
                                return ;
                        default :
                                // un choix non valide
                                printf("\n Le choix introduit n'est pas valide ! \n");
                                break;
                }
        }while(choix != 4); // boucler jusqu'à sortir
}

